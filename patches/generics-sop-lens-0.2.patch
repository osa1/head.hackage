diff --git a/generics-sop-lens.cabal b/generics-sop-lens.cabal
index bb40a60..2191a34 100644
--- a/generics-sop-lens.cabal
+++ b/generics-sop-lens.cabal
@@ -1,40 +1,41 @@
-name:           generics-sop-lens
-version:        0.2
-synopsis:       Lenses for types in generics-sop
-description:    Lenses for types in generics-sop package
-category:       Generics, Data
-homepage:       https://github.com/phadej/generics-sop-lens#readme
-bug-reports:    https://github.com/phadej/generics-sop-lens/issues
-author:         Oleg Grenrus <oleg.grenrus@iki.fi>
-maintainer:     Oleg Grenrus <oleg.grenrus@iki.fi>
-license:        BSD3
-license-file:   LICENSE
+name:               generics-sop-lens
+version:            0.2
+x-revision:         1
+synopsis:           Lenses for types in generics-sop
+description:        Lenses for types in generics-sop package
+category:           Generics, Data
+homepage:           https://github.com/phadej/generics-sop-lens#readme
+bug-reports:        https://github.com/phadej/generics-sop-lens/issues
+author:             Oleg Grenrus <oleg.grenrus@iki.fi>
+maintainer:         Oleg Grenrus <oleg.grenrus@iki.fi>
+license:            BSD3
+license-file:       LICENSE
 tested-with:
-  GHC==7.8.4,
-  GHC==7.10.3,
-  GHC==8.0.2,
-  GHC==8.2.2,
-  GHC==8.4.4,
-  GHC==8.6.5
-build-type:     Simple
-cabal-version:  >= 1.10
+  GHC ==7.8.4
+   || ==7.10.3
+   || ==8.0.2
+   || ==8.2.2
+   || ==8.4.4
+   || ==8.6.5
+   || ==8.8.1
 
+build-type:         Simple
+cabal-version:      >=1.10
 extra-source-files:
-    CHANGELOG.md
-    README.md
+  CHANGELOG.md
+  README.md
 
 source-repository head
-  type: git
+  type:     git
   location: https://github.com/phadej/generics-sop-lens
 
 library
-  hs-source-dirs:
-      src
-  ghc-options: -Wall
+  hs-source-dirs:   src
+  ghc-options:      -Wall
   build-depends:
-      base                  >=4.7      && <4.13
-    , generics-sop          >=0.2      && <0.6
-    , lens                  >=4.7      && <4.18
-  exposed-modules:
-      Generics.SOP.Lens
+      base          >=4.7 && <4.14
+    , generics-sop  >=0.2 && <0.6
+    , lens          >=4.7 && <4.19
+
+  exposed-modules:  Generics.SOP.Lens
   default-language: Haskell2010
diff --git a/src/Generics/SOP/Lens.hs b/src/Generics/SOP/Lens.hs
index e789929..954805d 100644
--- a/src/Generics/SOP/Lens.hs
+++ b/src/Generics/SOP/Lens.hs
@@ -90,25 +90,25 @@ productRep = rep . sop . nsSingleton
 -- >>> Just 'x' ^. rep . sop
 -- S (Z (I 'x' :* Nil))
 sop ::
-    forall (f :: k -> *) xss yss.
+    forall k (f :: k -> *) xss yss.
     Iso (SOP f xss) (SOP f yss) (NS (NP f) xss) (NS (NP f) yss)
 sop = iso unSOP SOP
 
 -- | Alias for 'sop'.
 _SOP ::
-    forall (f :: k -> *) xss yss.
+    forall k (f :: k -> *) xss yss.
     Iso (SOP f xss) (SOP f yss) (NS (NP f) xss) (NS (NP f) yss)
 _SOP = sop
 
 -- | The only field of 'POP'.
 pop ::
-    forall (f :: k -> *) xss yss.
+    forall k (f :: k -> *) xss yss.
     Iso (POP f xss) (POP f yss) (NP (NP f) xss) (NP (NP f) yss)
 pop = iso unPOP POP
 
 -- | Alias for 'pop'.
 _POP ::
-    forall (f :: k -> *) xss yss.
+    forall k (f :: k -> *) xss yss.
     Iso (POP f xss) (POP f yss) (NP (NP f) xss) (NP (NP f) yss)
 _POP = pop
 
@@ -147,7 +147,7 @@ instance Wrapped (K a b) where
 -------------------------------------------------------------------------------
 
 npSingleton ::
-    forall (f :: k -> *) x y.
+    forall k (f :: k -> *) x y.
     Iso (NP f '[x]) (NP f '[y]) (f x) (f y)
 npSingleton = iso g s
   where
@@ -169,7 +169,7 @@ instance (xs ~ '[x]) => Wrapped (NS f xs) where
     _Wrapped' = nsSingleton
 
 npHead ::
-    forall (f :: k -> *) x y zs.
+    forall k (f :: k -> *) x y zs.
     Lens (NP f (x ': zs)) (NP f (y ': zs)) (f x) (f y)
 npHead = lens g s
   where
@@ -180,7 +180,7 @@ npHead = lens g s
     s (_x :*  zs) y = y :* zs
 
 npTail ::
-    forall (f :: k -> *) x ys zs.
+    forall k (f :: k -> *) x ys zs.
     Lens (NP f (x ': ys)) (NP f (x ': zs)) (NP f ys) (NP f zs)
 npTail = lens g s
   where
@@ -222,7 +222,7 @@ instance Field9 (POP f' (a ': b ': c ': d ': e ': f ': g ': h ': x ': zs)) (POP
 -------------------------------------------------------------------------------
 
 nsSingleton ::
-    forall (f :: k -> *) x y.
+    forall k (f :: k -> *) x y.
     Iso (NS f '[x]) (NS f '[y]) (f x) (f y)
 nsSingleton = iso g Z
   where
@@ -235,7 +235,7 @@ nsSingleton = iso g Z
 #endif
 
 _Z ::
-    forall (f :: k -> *) x y zs.
+    forall k (f :: k -> *) x y zs.
     Prism (NS f (x ': zs)) (NS f (y ': zs)) (f x) (f y)
 _Z = prism Z p
   where
@@ -244,7 +244,7 @@ _Z = prism Z p
     p (S xs) = Left (S xs)
 
 _S ::
-    forall (f :: k -> *) x ys zs.
+    forall k (f :: k -> *) x ys zs.
     Prism (NS f (x ': ys)) (NS f (x ': zs)) (NS f ys) (NS f zs)
 _S = prism S p
   where
